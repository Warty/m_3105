#!/usr/bin/env bash

#************************NOTE*******************
#Pour chaque serveur que le script déploie dans la topologie, il faut exécuter la commande named -c /etc/named.conf afin de procèder aux
#démarrage des serveurs
#************************FIN NOTE*******************

#**************Partie 2 : Authoritative Server************
#Configuration dwikiorg
himage dwikiorg mkdir -p /etc/named #Ici on crée le fichier named dans /etc avec mkdir
hcp dwikiorg/* dwikiorg:/etc/named/. 
# Ici on copie (avec hcp) tout ce qu'il y'a dans le dossier locale avec l'attribut *  dans dwikiorg, et cole dans le dossier named du
#server dwikiorg
hcp dwikiorg/named.conf dwikiorg:/etc/.

#Configuration serveur diutre
himage diutre mkdir -p /etc/named
hcp diutre/* diutre:/etc/named/.
hcp diutre/named.conf diutre:/etc/.
himage diutre mkdir -p /tmp/www

#Configuration drtiutre
himage drtiutre mkdir -p /etc/named
hcp drtiutre/* drtiutre:/etc/named/.
hcp drtiutre/named.conf drtiutre:/etc/.
#***********Fin Partie 2 : Configuration Serveur Autorité***********

#***********Partie 3 : Top Level Domain*****************************
#Configuration pour le serveur dorg
himage dorg mkdir -p /etc/named
hcp dorg/* dorg:/etc/named/.
hcp dorg/named.conf dorg:/etc/.
#***********Fin Partie 3 : Top Level Domain*************************

#Configuration pour le serveur dre
himage dre mkdir -p /etc/named
hcp dre/* dre:/etc/named/.
hcp dre/named.conf dre:/etc/.
#*************Fin Partie 3 : Top level Domain**************************

#*************Partie 4 : Root Serveur**********************************
#Configuration aRootServer
himage aRootServer mkdir -p /etc/named
hcp aRootServer/* aRootServer:/etc/named/.
hcp aRootServer/named.conf aRootServer:/etc/.
#*************Fin Partie 4 RootServer***********************************
#*********************CONFIGURATION DES PCs**********************************
#Config pc1
hcp pc1/resolv.conf pc1:/etc/.
#Copie de revolv.conf dans etc du PC1
#Config pc2
hcp pc2/resolv.conf pc2:/etc/.
#Copie de revolv.conf dans etc du PC2
